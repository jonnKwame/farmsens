import React from 'react';
import '../assets/css/partners.css';

export default function OurPartners(){
    return (
        <section id="partners">
          <div className="container">
            <div className="intro">
              <h2>Our Partners</h2>
              <p>
                These are the organizations who believe in our hardwork.
              </p>
            </div>
            <div className="row no-gutters  clearfix wow fadeInUp">
    
              <div className="col-lg-3 col-md-6 col-3">
                <div className="partner-logo">
                  <img src={require('../assets/img/partners/csir.png')} className="img-fluid" alt="" data-aos="flip-right"/>
                </div>
              </div>
    
              <div className="col-lg-3 col-md-6 col-3">
                <div className="partner-logo">
                  <img src={require('../assets/img/partners/wazi.png')} className="img-fluid" alt="" data-aos="flip-right" data-aos-delay="100"/>
                </div>
              </div>
    
              <div className="col-lg-3 col-md-6 col-3">
                <div className="partner-logo">
                  <img src={require('../assets/img/partners/khive.jpg')} className="img-fluid" alt="" data-aos="flip-right" data-aos-delay="200"/>
                </div>
              </div>
    
              <div className="col-lg-3 col-md-6 col-3">
                <div className="partner-logo">
                  <img src={require('../assets/img/partners/mofa.png')} className="img-fluid" alt="" data-aos="flip-right" data-aos-delay="300"/>
                </div>
              </div>
    
              
    
            </div>
    
          </div>
        </section>


    )
}