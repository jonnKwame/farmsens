import React from 'react';
import {Link} from 'react-router-dom';

const Footer = () =>{
    return (
        <footer id="footer">
                <div className="footer-top">
                <div className="container">
                    <div className="row">

                    <div className="col-lg-4 col-md-6 footer-info">
                        <h3>FarmSens</h3>
                        <p>
                        Farmsens was built to help remote and local smallholder farmers who cannot
                        afford soil testing services and extension services, but want to increase their yields.
                        </p>
                        <br/>
                       <Link to="/about" className="aa"> Learn more </Link>
                    </div>

                    <div className="col-lg-4 col-md-6 footer-links">
                        <h4>Quick Links</h4>
                        <ul>
                        <li> <Link to="/product"> FarmSens Community</Link>  </li> 
                        <li> <Link to="/about"> Careers</Link>   </li> 
                        <li> <Link to="/about"> Terms of Service</Link> </li> 
                        <li> <Link to="/product"> Privacy Policy</Link> </li> 
                        </ul>
                    </div>

                    <div className="col-lg-4 col-md-6 footer-contact">
                        <h4>Contact Us</h4>
                        <p>
                        <strong>Phone:</strong> +233 (0)54 346 06333<br/>
                        <strong>Email:</strong> info@farmsens.com<br/><br/>
                        Kumasi Hive <br/>
                        Kumasi, Ashanti Region<br/>
                        Ghana, West Africa <br/>
                        </p>

                        <div className="social-links">
                        <a href="www.twitter.com" className="twitter"><i className="bx bxl-twitter"></i></a>
                        <a href="www.facebook.com" className="facebook"><i className="bx bxl-facebook"></i></a>
                        <a href="www.instagram.com" className="instagram"><i className="bx bxl-instagram"></i></a>
                        
                        <a href="www.linkedin.com" className="linkedin"><i className="bx bxl-linkedin"></i></a>            
                        </div>

                    </div>

                    </div>
                </div>
                </div>

                <div className="container">
                <div className="copyright">
                    &copy; Copyright 2020  <strong><span>FarmSens</span></strong>. All Rights Reserved
                </div>
                </div>
        </footer>
    );
};
export default Footer;
