import React from 'react';
import { NavLink} from 'react-router-dom';

const Header = () =>{
    

    return (
        <header className="header_area" id="header">
		<div className="container main_menu">
			<nav className="navbar navbar-expand-lg navbar-light">
				<div className="container">
					<a className="navbar-brand logo_h" href="/"><img src="./logo.png" alt=""/></a>
					<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span className="icon-bar"><i className="icofont-navigation-menu"></i></span>
						<span className="icon-bar"></span>
						<span className="icon-bar"></span>
					</button>
					
                    <div className="collapse navbar-collapse offset" id="navbarSupportedContent">                      
                        <ul className="nav navbar-nav menu_nav justify-content-end">
							<li className="nav-item"><a className="nav-link" href="/">Home</a></li>
							<li className="nav-item">
                            <NavLink className="nav-link" activeClassName="active" to="/product">  Product  </NavLink>
                            </li>
                            <li className="nav-item">
                            <NavLink className="nav-link" activeClassName="active" to="/about">  About Us  </NavLink>
                            </li>
                            <li className="nav-item">
                            <NavLink className="nav-link" activeClassName="active" to="/store">  Store  </NavLink>
                            </li>
                            {/*<li className="nav-item">
                            <NavLink className="nav-link" activeClassName="active" to="/blog">  Blog  </NavLink>
							</li>*/}
						</ul>
                        
                        {/*<a href="#about" className="get-started-btn scrollto">Sign In</a>*/}
						<NavLink className="get-started-btn scrollto" to="/account">  Sign In  </NavLink>
					</div>
				</div>
			</nav>
		</div>
	    </header>

							

    );

};
export default Header;

