import React, {Component} from 'react';
import {Link} from 'react-router-dom';





class Product extends  Component {

    render() {
        return (
            <React.Fragment>
                
                <section id='hero2'>
                    <div className="hero-container"  >
                     
                        <h2>Measure, Analyse, Take actions </h2>
                        <p>Easy to install soil sensors with real time comprehensible data analytics.</p>
                        <div className="d-flex">
                        <Link to="/about" className="btn-get-started scrollto"> Request a demo </Link> 
                        </div> 
                    
                    </div>
                </section>

                

                
                
                

                

            



                <section id="contact" className="contact">
                    <div className="container" >

                        <div className="section-title">
                        <h4>
                        We are committed to providing local smallholder farmers with portable and affordable technologies to enable them make smarter and better decisions along the agricultural value chain. 
                        Our technologies ensure farmers minimize waste of farm resources while increasing thier yields
                        </h4>
                        <br/>
                        <br/>
                        <Link to="/about" className="get-started-btn scrollto"> Learn more!</Link> 
                        </div>
                    </div>
                </section>


        


                <section  className="tabs">
                    <div className="container" >
                        <div className="section-title">
                           
                            
                           

                        </div>

                        <div className="tab-content">
                        <div className="tab-pane active show">
                            <div className="row">
                            <div className="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0"  >
                            <div>
                                <h3>Breakthrough in soil testing...</h3>
                                
                                <p>
                                    FarmSens provides wireless and modular soil sensors to monitor varying 
                                    soil parameters of crucial importance to soil fertility and crop productivity;
                                </p>
                                <ul>
                                <li><i className="ri-check-double-line"></i> soil moisture</li>
                                <li><i className="ri-check-double-line"></i> soil nutrients levels</li>
                                <li><i className="ri-check-double-line"></i> soil pH</li>
                                <li><i className="ri-check-double-line"></i> soil electrical conductivity.</li>
                                <li><i className="ri-check-double-line"></i> soil soil temperature</li>
                                <li><i className="ri-check-double-line"></i> air temperature and humidity</li>
                                </ul>
                                <p>
                                The sensors come in the form of kits. They form networks with each other, with one as a master and the rest as slaves.
                                The master sensor serves as the gateway, through which the data is sent to the cloud via cellular connectivity.
                                
                                </p>
                                
                            </div>
                            </div>
                            <div className="col-lg-6 order-1 order-lg-2 text-center"  >
                                <img src={require('../assets/img/IMG-20200731-WA0008.jpg')} alt="" className="img-fluid"/>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </section>


                <section id="vid">
                    <center>
                        
                        <video  controls>
                            <source  src={require('../assets/img/VID-20200804-WA0009.mp4')} type="video/mp4"/> 
                        </video>
                    
                    </center>
                </section>
    
                
   

                <section>
                    <div className="container">
                        <div className="section-title">
                            <h2>Smart application with value based services</h2>
                        </div>

                        <div>
                            <p>
                                Our mobile applications enble you to monitor your farm operations ...
                                Download now and receive weekly, real-time soil status information and
                                recommendations, which empowers you to make better decisions along the agricultural value chain, irrespective of their
                                farm sizes, literacy, and locations.
                            </p>
                            <p>
                                Download now and join the largest farmer community platform connecting all farmers, agricultural experts, credit
                                providers, input sources, and market sources together.
                                Ask questions, upload pictures of pests or diseases affecting your crops, and receive instant advice from the experts.
                                
                                Input providers will connect directly with farmers and provide them with quality and affordable farm inputs.
                                Market experts will reach out to the producers and educate them on the quality of produce they need from them.
                                This community platform is also accessible via USSD.
                                
                            </p>
                            <p>
                                Download now and experience a new world of farming
                            </p>
                        </div>

                        <div className="section-title">
                            <Link to="/product" className="get-started-btn scrollto"> Download Now </Link> 
                        </div>
                    

                        <div className="row">

                        <div className="col-md-4">
                            <div className="about-col">
                            <div className="img">
                                <img src={require('../assets/img/app screenshot5.png')}  alt="" className="img-fluid" /> 
                            </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="about-col">
                            <div className="img">
                                <img src={require('../assets/img/app screenshot6.png')}  alt="" className="img-fluid" /> 
                            </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="about-col">
                            <div className="img">
                                <img src={require('../assets/img/app screenshot4.png')}  alt="" className="img-fluid" /> 
                            </div>
                            </div>
                        </div>


                        </div>
                        <br/><br/>
                        
                        
                        

                    </div>
                </section>
  



                <section id="clients" class="clients">
                    <div class="container">
                        <div className="section-title">
                            <h2>Our Patners</h2>


                            <p> Become a <Link to="/about">Partner</Link></p>
                            
                        </div>

                        <div class="row">

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/khive.jpg')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/wazi.png')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/mofa.png')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/giz.jpg')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/csir.png')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/img.jpg')} alt=""/>
                        </div>

                        </div>

                    </div>
                </section>
                

                

            </React.Fragment>
            
        );
    }
    

}

export default Product;