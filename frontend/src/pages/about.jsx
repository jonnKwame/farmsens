import React, {Component} from 'react';
import {Link} from 'react-router-dom';





class About extends  Component {

    render() {
        return (
            <React.Fragment>
                
                <section id='hero2'>
                    <div className="hero-container"  >
                     
                        
                        <p>We at farmsens are committed to providing local smallholder farmers with portable and affordable technologies to enable them make smarter and better decisions along the agricultural value chain. 
                        Our technologies ensure farmers minimize waste of farm resources while increasing thier yields</p>
                        <div className="d-flex">
                        <Link to="/about" className="btn-get-started scrollto"> Request a demo </Link> 
                        </div> 
                    
                    </div>
                </section>

                

                
                
                

                

            



                <section id="contact" className="contact">
                    <div className="container" >

                        <div className="section-title">
                        <h4>
                        We are committed to providing local smallholder farmers with portable and affordable technologies to enable them make smarter and better decisions along the agricultural value chain. 
                        Our technologies ensure farmers minimize waste of farm resources while increasing thier yields
                        </h4>
                        <br/>
                        <br/>
                        <Link to="/about" className="get-started-btn scrollto"> Learn more!</Link> 
                        </div>
                    </div>
                </section>


                <section id="team" class="team section-bg">
                    <div class="container" data-aos="fade-up">

                        <div class="section-title">
                        <h2>Team</h2>
                        <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.</p>
                        </div>

                        <div class="row">

                        <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                            <div class="member" data-aos="fade-up" data-aos-delay="100">
                            <div class="member-img">
                                <img src={require('../assets/img/team/john.jpg')} class="img-fluid" alt=""/>
                            </div>
                            <div class="member-info">
                                <h4>John Kwame Dunyo</h4>
                                <span>Team Lead</span>
                            </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                            <div class="member" data-aos="fade-up" data-aos-delay="200">
                            <div class="member-img">
                                <img src={require('../assets/img/team/francis.jpg')} class="img-fluid" alt=""/>
                            </div>
                            <div class="member-info">
                                <h4>Francis Gyimah</h4>
                                <span>Software Engineer</span>
                            </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                            <div class="member" data-aos="fade-up" data-aos-delay="300">
                            <div class="member-img">
                                <img src={require('../assets/img/team/kevin.jpg')} class="img-fluid" alt=""/>
                            </div>
                            <div class="member-info">
                                <h4>Raymond Obu</h4>
                                <span>Hardware Engineer</span>
                            </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                            <div class="member" data-aos="fade-up" data-aos-delay="400">
                            <div class="member-img">
                                <img src={require('../assets/img/team/abraham.jpg')} class="img-fluid" alt=""/>
                            </div>
                            <div class="member-info">
                                <h4>Abraham Kudiabor</h4>
                                <span>Marketing and Bussiness Developer</span>
                            </div>
                            </div>
                        </div>

                        </div>

                    </div>
                </section>
        


                      
                        
                <section id="ourMotivation">
                    <div className="container">

                        <center className="section-title" >
                        <h2>Our Impacts</h2>
                        
                        </center>

                        <div className="row about-cols">

                        <div className="col-md-4 wow fadeInUp">
                            <div className="about-col">
                            <div className="img">
                            
                                <img src={require('../assets/img/goal1.png')}  alt="" className="img-fluid" />
                                
                            </div>
                            
                            <p>
                                We are committed to providing decent livelihood for farmers
                            </p>
                            </div>
                        </div>

                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
                            <div className="about-col">
                            <div className="img">
                                <img src={require('../assets/img/goal2.png')}  alt="" className="img-fluid" />
                                
                            </div>
                            
                            <p>
                                We are commited to feeding the world’s growing populations
                            </p>
                            </div>
                        </div>

                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
                            <div className="about-col">
                            <div className="img">
                                <img src={require('../assets/img/goal13.png')}  alt="" className="img-fluid" />
                                
                            </div>
                            
                            <p>
                                We are commited to protecting the environment
                            </p>
                            </div>
                        </div>

                        </div>

                    </div>
                </section>




                <section id="clients" class="clients">
                    <div class="container">
                        <div className="section-title">
                            <h2>Our Patners</h2>


                            <p> Become a <Link to="/about">Partner</Link></p>
                            
                        </div>

                        <div class="row">

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/khive.jpg')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/wazi.png')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/mofa.png')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/giz.jpg')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/csir.png')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/img.jpg')} alt=""/>
                        </div>

                        </div>

                    </div>
                </section>
                

                

            </React.Fragment>
            
        );
    }
    

}

export default About;