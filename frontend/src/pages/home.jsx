import React, {Component} from 'react';
import {Link} from 'react-router-dom';





class Home extends  Component {

    render() {
        return (
            <React.Fragment>
                {/*  
                <section id='hero'>
                    <div className="hero-container"  >
                     
                        <h1>Measure, Analyse, Take actions </h1>
                        <h2>Easy to install soil sensors with real time comprehensible data analytics.</h2>
                        <div className="d-flex">
                        <Link to="/about" className="get-started-btn scrollto"> View Documentations!</Link> 
                        <a href="/" className="venobox btn-watch-video" data-vbtype="video" data-autoplay="true"> Watch Video <i className="icofont-play-alt-2"></i></a>
                    </div> 
                    
                    </div>
                </section>

                */}

                <section id="hero">
                    <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

                    <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

                    <div class="carousel-inner" role="listbox">

                       
                        <div class="carousel-item active" >
                            <div className="carousel-background"><img src={require('../assets/img/soil5.png')}  alt=""/></div>
                        <div class="carousel-container">
                            <div class="container">
                            <h2 class="animate__animated animate__fadeInDown">Measure, Analyse, Take actions</h2>
                            <p class="animate__animated animate__fadeInUp">
                            Easy to install soil sensors with real time comprehensible data analytics specific to your farm.
                            </p>
                            <Link to="/product" className="btn-get-started animate__animated animate__fadeInUp scrollto">View Documentations</Link>

                            <Link to="/store"  className="btn-get-started animate__animated animate__fadeInUp scrollto">Request a demo</Link>
                            </div>
                        </div>
                        </div>

                        
                        <div class="carousel-item" >
                            <div className="carousel-background"><img src={require('../assets/img/bg_1.jpg')}  alt=""/></div>
                        <div class="carousel-container">
                            <div class="container">
                            <h2 class="animate__animated animate__fadeInDown">Enhancing precision farming among farmers</h2>
                            <p class="animate__animated animate__fadeInUp">
                                empowering smallholder farmers with affordable innovative technologies
                                to make better decisions, access resources to improve yields and reduce waste,
                                towards sustainable food production and poverty reduction    
                            </p>
                            <Link to="/product"  className="btn-get-started animate__animated animate__fadeInUp scrollto">Learn More</Link>
                            </div>
                        </div>
                        </div>

                        
                        <div class="carousel-item" >
                            <div className="carousel-background"><img src={require('../assets/img/bg_2.jpg')}  alt=""/></div>
                        <div class="carousel-container">
                            <div class="container">
                            <h2 class="animate__animated animate__fadeInDown">Harnessing agricultural potentials</h2>
                            <p class="animate__animated animate__fadeInUp">
                                Farmer friendly IoT platform with real time farm monitoring and actionalble data analytics to enable farmers
                                farm better. Our platform enables farmers sell thier produce at competitive prices.
                            </p>
                            <Link to="/product"  className="btn-get-started animate__animated animate__fadeInUp scrollto">Get Started</Link>
                            </div>
                        </div>
                        </div>

                    </div>

                    <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon icofont-simple-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>

                    <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon icofont-simple-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>

                    </div>
                </section>
                
                {/*}
                <section id="intro">
                    <div className="intro-container">
                    <div id="introCarousel" className="carousel  slide carousel-fade" data-ride="carousel">

                        <ol className="carousel-indicators"></ol>

                        <div className="carousel-inner" role="listbox">

                        <div className="carousel-item active">
                            <div className="carousel-background"><img src={require('../assets/img/soil5.png')}  alt=""/></div>
                            <div className="carousel-container">
                            <div className="carousel-content">
                                <h2>Measure, Analyse, Take actions</h2>
                                <p>Easy to install soil sensors with real time comprehensible data analytics specific to your farm.</p>
                                <Link to="/about" className="btn-get-started scrollto">View Documentations</Link>

                                <Link to="/store"  className="btn-get-started scrollto">Buy a sensor kit</Link>
                            </div>
                            </div>
                        </div>

                        <div className="carousel-item">
                            <div className="carousel-background"><img src={require('../assets/img/bg_1.jpg')}  alt=""/></div>
                            <div className="carousel-container">
                            <div className="carousel-content">
                                <h2>Enhancing precision farming among farmers</h2>
                                <p>
                                    empowering smallholder farmers with affordable innovative technologies
                                    to make better decisions, access resources to improve yields and reduce waste,
                                    towards sustainable food production and poverty reduction    
                                </p>
                                <a href="#featured-services" className="btn-get-started scrollto">Get Started</a>
                            </div>
                            </div>
                        </div>

                        <div className="carousel-item">
                            <div className="carousel-background"><img src={require('../assets/img/bg_2.jpg')}  alt=""/></div>
                            <div className="carousel-container">
                            <div className="carousel-content">
                                <h2>Optimizing opportunities in the agricultural sectors</h2>
                                <p>Beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt omnis iste natus error sit voluptatem accusantium.</p>
                                <a href="#featured-services" className="btn-get-started scrollto">Get Started</a>
                            </div>
                            </div>
                        </div>

                        </div>

                        <a className="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
                        <span className="sr-only">Previous</span>
                        </a>

                        <a className="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
                        <span className="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
                        <span className="sr-only">Next</span>
                        </a>

                    </div>
                    </div>
                </section>
                */}



                <section id="contact" className="contact">
                    <div className="container" >

                        <div className="section-title">
                        <h4>
                        We at FarmSens are committed to providing local smallholder farmers with portable and affordable technologies to enable them make smarter and better decisions along the agricultural value chain. 
                        Our technologies ensure farmers minimize waste of farm resources while increasing thier yields
                        </h4>
                        <br/>
                        <br/>
                        <Link to="/about" className="get-started-btn scrollto"> Our Story!</Link> 
                        </div>
                    </div>
                </section>


                <section id="services" className="services section-bg ">
                    <div className="container" >

                        <div className="section-title">
                        <h2>Our Services</h2>
                        <p> FarmSens empowers smallholder farmers with data analytics for key decisions
                            and access to services along the entire value chain. Our services are accessible via the FarmSens app and USSD</p>
                        </div>

                        <div className="row">
                        <div className="col-md-6">
                            <div className="icon-box"  >
                            <i className="icofont-computer"></i>
                            <h4>Soil Testing</h4>
                            <p>
                                Our affordable, portable and highly sensitive soil testing device provides farmers 
                                with real time comprehensible soil test information that reflects the true nature of their soils. 
                                These sensors are easy to install and work in all most all agricultural settings.
                            </p>
                            </div>
                        </div>
                        <div className="col-md-6 mt-4 mt-md-0">
                            <div className="icon-box"  >
                            <i className="icofont-chart-bar-graph"></i>
                            <h4>Extension Advice</h4>
                            <p>
                                FarmSens provides farmers with receive real-time actionable insights on their farm status. 
                                They will use the right amount of fertilizers and other additives, and better combat pests and diseases. 
                                This reduces guesswork, maximizes efficiency while reducing wastes
                            </p>
                            </div>
                        </div>
                        <div className="col-md-6 mt-4 mt-md-0">
                            <div className="icon-box"  >
                            <i className="icofont-image"></i>
                            <h4>Farmer Profiles</h4>
                            <p>
                                FarmSens accesses farmers' risks through predicting harvest time, 
                                quantity and quality of harvests, and their ability to sell. These data, coupled with previous financial 
                                histories are used in our credit scoring models to build farmer profiles, to facilitate credit applications
                            </p>
                            </div>
                        </div>
                        <div className="col-md-6 mt-4 mt-md-0">
                            <div className="icon-box"  >
                            <i className="icofont-settings"></i>
                            <h4>Access to Markets</h4>
                            <p>
                                FarmSens bridges the gap between farmers and agricultural input sources, and the market by
                                offering such collective information on a single platform.

                                We connect farmers directly to buyers, thust ensuring a competitive prices for all produce
                            </p>
                            </div>
                        </div>
                        
                        </div>

                    </div>
                </section>


                <section  className="tabs">
                    <div className="container" >
                        <div className="section-title">
                            <h2>Our Solution</h2>
                            
                            <p> FarmSens is a farmer-friendly IoT farming platform that combines real-time data to provide tailored actionable insights
                                coupled with extension advice, build farmer profiles to guarantee credits, connect farmers to markets and storage
                                facilities, and other services along the entire value chain.
 
                            </p>

                        </div>

                        <div className="tab-content">
                        <div className="tab-pane active show">
                            <div className="row">
                            <div className="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0"  >
                            <div>
                                <h3>Breakthrough in soil testing...</h3>
                                
                                <p>
                                    FarmSens provides wireless and modular soil sensors to monitor varying 
                                    soil parameters of crucial importance to soil fertility and crop productivity;
                                </p>
                                <ul>
                                <li><i className="ri-check-double-line"></i> soil moisture</li>
                                <li><i className="ri-check-double-line"></i> soil nutrients levels</li>
                                <li><i className="ri-check-double-line"></i> soil pH</li>
                                <li><i className="ri-check-double-line"></i> soil electrical conductivity.</li>
                                <li><i className="ri-check-double-line"></i> soil soil temperature</li>
                                <li><i className="ri-check-double-line"></i> air temperature and humidity</li>
                                </ul>
                                <p>
                                The sensors come in the form of kits. They form networks with each other, with one as a master and the rest as slaves.
                                The master sensor serves as the gateway, through which the data is sent to the cloud via cellular connectivity.
                                
                                </p>
                                <Link to="/product" className="get-started-btn"> Learn more!</Link>
                            </div>
                            </div>
                            <div className="col-lg-6 order-1 order-lg-2 text-center"  >
                                <img src={require('../assets/img/IMG-20200731-WA0008.jpg')} alt="" className="img-fluid"/>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </section>

                <section  className="tabs">
                    <div className="container" >
                        
                        <div className="tab-content">
                        <div className="tab-pane active">
                            <div className="row">

                            <div className="col-lg-6 order-1 order-lg-2 mt-3 mt-lg-0">
                                <h3>Precision farming based on real time soil nutrients status</h3>
                                <p>
                                Data from the sensors are analyzed by our tested machine learning algorithms
                                to provide farmers with actionable recommendations on....
                                </p>
                                <ul>
                                <li><i className="ri-check-double-line"></i> Suitable crops and fertilizer applications</li>
                                <li><i className="ri-check-double-line"></i> Irrigation and resilient cultivation practises.</li>
                                <li><i className="ri-check-double-line"></i> Pests and disease control, and mitigation practises.</li>
                                </ul>
                                <p>
                                FarmSens is trying to change the way agricultural operations are carried out in Africa by incorporating real-time filed
                                data to reduce guesswork, maximize efficiency while reducing wastes, and thus enhancing African farmers’ ability to
                                feed the world.
                                </p>
                            </div>
                            <div className="col-lg-6 order-2 order-lg-1 text-center">
                                <img src={require('../assets/img/left-image.png')}  alt="" className="img-fluid"/>
                            </div>
                            </div>
                        </div>
                        </div>

                    </div>
                </section>
                
                {/** 
                <section  className="tabs">
                    <div className="container" >
                        <div className="tab-content">
                        <div className="tab-pane active show">
                            <div className="row">
                            <div className="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0"  >
                                <img src={require('../assets/img/IMG-20200731-WA0004.jpg')} alt="" className="img-fluid"/>
                            </div>
                            <div className="col-lg-6 order-1 order-lg-2"  >
                                <img src={require('../assets/img/IMG-20200731-WA0004.jpg')} alt="" className="img-fluid"/>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </section>
                */}

                <section id="portfolio" className="portfolio">
                    <div className="container">
                    <div className="row">
                        <div className="col-lg-6 col-md-6 portfolio-item ">
                            <div className="portfolio-wrap">
                            <img src={require('../assets/img/IMG-20200731-WA0004.jpg')} className="img-fluid" alt=""/>
                            <div className="portfolio-info">
                                <p>Prototype 1</p>
                                <div className="portfolio-links">
                                <a href={require('../assets/img/IMG-20200731-WA0004.jpg')}  data-gall="portfolioGallery" className="venobox" title="Prototype 1"><i className="bx bx-plus"></i></a>
                                
                                </div>
                            </div>
                            </div>
                        </div>

                        <div className="col-lg-6 col-md-6 portfolio-item ">
                            <div className="portfolio-wrap">
                            <img src={require('../assets/img/IMG-20200731-WA0004.jpg')} className="img-fluid" alt=""/>
                            <div className="portfolio-info">
                                <p>Prototype 1</p>
                                <div className="portfolio-links">
                                <a href={require('../assets/img/IMG-20200731-WA0014.jpg')} data-gall="portfolioGallery" className="venobox" title="Prototype 1"><i className="bx bx-plus"></i></a>
                                
                                </div>
                            </div>
                            </div>
                        </div>

                        
                    </div>

                    </div>
                </section>


                

                <section id="faq" className="faq">
                    <div className="container" >

                        <div className="section-title">
                        <h2>Frequently Asked Questions</h2>
                        </div>

                        <ul className="faq-list" >

                        <li>
                            <a data-toggle="collapse" className="collapsed" href="#faq1">Q: How does FarmSens work? <i className="bx bx-chevron-down icon-show"></i><i className="bx bx-x icon-close"></i></a>
                            <div id="faq1" className="collapse" data-parent=".faq-list">
                            <p>
                                FarmSens uses real time data from soil sensors to offer actionable insights to farmers.
                                You just need to purchase FarmSens sensor kit and install them in your farm.
                                The sensors are easy to install.
                            </p>
                            <p>
                                Then you download the farmSens app from the playstore, create an account and 
                                add the sensors by scanning the qr code on them. 
                                Next thing is add the crops you intend cultivating.
                                Thats all!                                
                            </p>
                            <p>
                                FarmSens will use the real time data from your farm to recommend suitable fertilizers
                                and extension advice on resilient cultivaton practises. 
                                This is to enable you achieve high yields with fewer resources.

                                FarmSens will then use data from your farms to build a farmer profile which you can use to guarantee loans.

                                FarmSens will also connect you directly to crop aggregators to enable  you sell you produce at competitive prices.                                
                            </p>
                            </div>
                        </li>

                        <li>
                            <a data-toggle="collapse" href="#faq2" className="collapsed">Q: How do I use FarmSens if I do not have a smartphone? <i className="bx bx-chevron-down icon-show"></i><i className="bx bx-x icon-close"></i></a>
                            <div id="faq2" className="collapse" data-parent=".faq-list">
                            <p>
                                FarmSens was built with the needs of farmers at heart.

                                Farmers without smartphones will be able to access the services via USSD.
                            </p>
                            <p>
                                All you need to do is to indicate to the FarmSens agent you bought the sensor kits from.
                                The agent will creat an account for you, FarmSens will then provide all services to you via USSD  and automated voice calls.

                                FarmSens has field agents in all regions, they will be available to help you.
                            </p>
                            </div>
                        </li>


                        <li>
                            <a data-toggle="collapse" href="#faq3" className="collapsed">Q: How many models of the FarmSens Sensors are available? <i className="bx bx-chevron-down icon-show"></i><i className="bx bx-x icon-close"></i></a>
                            <div id="faq3" className="collapse" data-parent=".faq-list">
                            <p>
                                FarSens has two (2) models whose prototypes are currently being piloted. Concepts for these prototypes have been proven.
                                They are:
                            </p>
                            <p>
                                The firt model is a single portable hardware, robustly designed for in-situ measurement of preconfigured soil conditions.
                                It comes in the form of a detachable probe and a meter.
                                Data from the sensors in the probe are sent to the meter for visualization and storage.
                                It communicates via Bluetooth, WIFI or LORA with FarmSens mobile applications for data logging.
                            </p>
                            <p>
                                The second model is the FarmSens Sensors Kit. These sensors are installed at various locations in the client’s farm 
                                to continuously measure soil parameters of interest. 
                                They form networks with each other, with one as a master and the rest as slaves.
                                The master sensor serves as the gateway, through which the data is sent to the cloud via cellular connectivity.
                            </p>

                            </div>
                        </li>

                        <li>
                            <a data-toggle="collapse" href="#faq4" className="collapsed">Q: Does the sensors need internet to work? <i className="bx bx-chevron-down icon-show"></i><i className="bx bx-x icon-close"></i></a>
                            <div id="faq4" className="collapse" data-parent=".faq-list">
                            <p>
                                No. The master sensor contains a SIM card, that enables it to send data via cellular connectivity to the cloud.
                                This is neccessary to ensure the sensors work without internet connectivity.
                            </p>
                            </div>
                        </li>

                    {/** 
                        <li>
                            <a data-toggle="collapse" href="#faq5" className="collapsed">Tortor vitae purus faucibus ornare. Varius vel pharetra vel turpis nunc eget lorem dolor? <i className="bx bx-chevron-down icon-show"></i><i className="bx bx-x icon-close"></i></a>
                            <div id="faq5" className="collapse" data-parent=".faq-list">
                            <p>
                                Laoreet sit amet cursus sit amet dictum sit amet justo. Mauris vitae ultricies leo integer malesuada nunc vel. Tincidunt eget nullam non nisi est sit amet. Turpis nunc eget lorem dolor sed. Ut venenatis tellus in metus vulputate eu scelerisque. Pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus faucibus. Nibh tellus molestie nunc non blandit massa enim nec.
                            </p>
                            </div>
                        </li>

                        <li>
                            <a data-toggle="collapse" href="#faq6" className="collapsed">Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi? <i className="bx bx-chevron-down icon-show"></i><i className="bx bx-x icon-close"></i></a>
                            <div id="faq6" className="collapse" data-parent=".faq-list">
                            <p>
                                Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
                            </p>
                            </div>
                        </li>
                    */}

                        </ul>

                    </div>
                </section>

                <section id="ourMotivation">
                    <div className="container">

                        <center className="section-title" >
                        <h2>Our Motivation</h2>
                        
                        </center>

                        <div className="row about-cols">

                        <div className="col-md-4 wow fadeInUp">
                            <div className="about-col">
                            <div className="img">
                            
                                <img src={require('../assets/img/goal1.png')}  alt="" className="img-fluid" />
                                
                            </div>
                            
                            <p>
                                We are committed to providing decent livelihood for farmers
                            </p>
                            </div>
                        </div>

                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
                            <div className="about-col">
                            <div className="img">
                                <img src={require('../assets/img/goal2.png')}  alt="" className="img-fluid" />
                                
                            </div>
                            
                            <p>
                                We are commited to feeding the world’s growing populations
                            </p>
                            </div>
                        </div>

                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
                            <div className="about-col">
                            <div className="img">
                                <img src={require('../assets/img/goal13.png')}  alt="" className="img-fluid" />
                                
                            </div>
                            
                            <p>
                                We are commited to protecting the environment
                            </p>
                            </div>
                        </div>

                        </div>

                    </div>
                </section>


                <section id="clients" class="clients">
                    <div class="container">
                        <div className="section-title">
                            <h2>Our Patners</h2>
                            <p> Become a <Link to="/about">Partner</Link></p>
                            
                        </div>

                        <div class="row">

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/khive.jpg')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/wazi.png')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/mofa.png')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/giz.jpg')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/csir.png')} alt=""/>
                        </div>

                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                            <img src={require('../assets/img/partners/img.jpg')} alt=""/>
                        </div>

                        </div>

                    </div>
                </section>
                

                

            </React.Fragment>
            
        );
    }
    

}

export default Home;