import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Helmet, HelmetProvider } from 'react-helmet-async';

import Header from './components/header';
import Footer from './components/footer';

import Home from './pages/home';
import Product from './pages/product';
import About from './pages/about';
import Store from './pages/store';
import Blog from './pages/blog';
import Account from './pages/account';


function App() {
  return (
    <HelmetProvider>
      <Helmet>
        {/* Default title when a page doesn't have a custom title(eg 404 pages, single prop page )  */}
					<title>FarmSens</title>
					<meta name="description" content="FarmSens" />
			</Helmet>
      <Router forceRefresh>
        <Header />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/product" exact component={Product} />
          <Route path="/about" exact component={About} />
          <Route path="/store" exact component={Store} />
          <Route path="/blog" exact component={Blog}/>
          <Route path="/account" exact component={Account}/>
        </Switch>
        <Footer />
      </Router>
    </HelmetProvider>
       
    
  );
}

export default App;
