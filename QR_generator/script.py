        # IF YOU ARE RUNNING DIRECTLY FROM THE REPO THEN JUST TYPE "source qr_gen/bin/activate" 

        # IF YOU AREN'T THEN FOLLOW THE SETUP GUIDE BELOW 
                        # Python script to generating UUIDs and QRCode
                        # Install packages for creating virtual envirnment by running "sudo pip3 install virtualenv "
                        # Create vurtual env "virtualenv name_of_virtual_env"
                        # Activate the virtual environment by running "source qr_gen/bin/activate"
                        # Install package "pip install qrcode[pil]"
                        # Install package "pip install pypng"

# After run this in your current directory "python script.py"
# to deactivate the virtual envirnoment type "deactivate"

                


import qrcode
import uuid
import png


            # Before you start, initialise the value in the count.txt file to a 4 digit integer
f_read= open("count.txt","r") 
f_uid = open("UIDs.txt","a")                                                  # Open the file before you read

def generate(int_x):                                                            # Function to generate Uuid and qr quode for a specifed number of iterations
        n= f_read.read()                                                        # Reading the last factory number of previous device
        x = 0
        for x in range(int_x):
                #device_uuid= uuid.uuid4()                                       # Creating the uuid using the inbuilt uuid function
                #print(device_uuid)                                              # Display the generated uuid
                device_id= "http://api.waziup.io/api/v2/devices/b827eb5ac836_"+ str(n)        # Concatinating the  factory index to the uuid generated 
                print(device_id + "\n")                                       # Display the new concated uid
                f_uid.write(device_id + "\n")
                img=qrcode.make(device_id)                                    # Generate QR for uuid
                img.save('img/image'+ str(n) + '.jpeg','JPEG')                  # Save unique image of each factory product to img directory
                n= int(n)+1                                                     # Increment the factory index by 1
                f_write= open("count.txt","w")                                  # Open the file to write to
                f_write.write(str(n))                                           # Write the last factory index to the file
                f_write.close()                                                 # Close the write file
             
        f_read.close()                                                          # Close the read file
        print("Completed")                                                      # Display the task has been completed


generate(4)                                                                    # Test case to produce 4 uids with corresponding qr code