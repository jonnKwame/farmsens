package io.computech.farmsens.fragments;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.sdsmdg.tastytoast.TastyToast;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import de.hdodenhof.circleimageview.CircleImageView;
import io.computech.farmsens.R;
import io.computech.farmsens.activities.StartActivity;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment implements View.OnClickListener {
    private CircleImageView userImageView;
    private RelativeLayout changeUserPhoto;
    private TextView nameTxt, emailTxt;
    private ImageView editName, editEmail;
    private ProgressBar loadingImageProgress;
    private Button logoutBtn;

    private String email,fullName;

    private FirebaseUser user;
    private FirebaseFirestore db;

    private static int PICK_IMAGE_REQUEST = 99;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // set action bar title
        requireActivity().setTitle("Profile");

        initUI(view);

        loadingImageProgress.setVisibility(View.VISIBLE);

        // initialize firebase
        user = FirebaseAuth.getInstance().getCurrentUser();
        assert user != null;

        db = FirebaseFirestore.getInstance();
        db.collection("users").document(user.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot doc = task.getResult();
                    assert doc != null;
                    fullName = doc.getString("fullName");
                    email = doc.getString("email");

                    nameTxt.setText(fullName);
                    emailTxt.setText(email);

                    if (doc.getString("photoUrl") != null) {
                        Glide.with(requireActivity())
                                .load(doc.getString("photoUrl"))
                                .into(userImageView);
                    }

                    loadingImageProgress.setVisibility(View.GONE);
                }
            }
        });

        changeUserPhoto.setOnClickListener(this);
        editName.setOnClickListener(this);
        editEmail.setOnClickListener(this);
        logoutBtn.setOnClickListener(this);
    }

    private void initUI(View view) {
        userImageView = view.findViewById(R.id.user_photo);
        changeUserPhoto = view.findViewById(R.id.change_photo);
        nameTxt = view.findViewById(R.id.name_txt);
        emailTxt = view.findViewById(R.id.email_txt);
        editEmail = view.findViewById(R.id.edit_email);
        editName = view.findViewById(R.id.edit_name);
        loadingImageProgress = view.findViewById(R.id.image_progress_bar);
        logoutBtn = view.findViewById(R.id.logoutBtn);
    }

    private void changePhoto() {
        Dexter.withActivity(requireActivity())
                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // Defining Implicit Intent to mobile gallery
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(
                                Intent.createChooser(
                                        intent,
                                        "Select Image from here..."),
                                PICK_IMAGE_REQUEST);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        TastyToast.makeText(requireContext(), "Storage permission needed to access image", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                                .show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        TastyToast.makeText(requireContext(), "Storage permission needed to access image", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                                .show();
                    }
                }).check();
    }

    private void changeDetails() {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        View dialogView = LayoutInflater.from(requireActivity()).inflate(
                R.layout.update_details_dialog,
                requireActivity().findViewById(R.id.updateDetailsDialog)
        );
        final EditText emailEdt = dialogView.findViewById(R.id.emailEdt);
        final EditText fullNameEdt = dialogView.findViewById(R.id.fullNameEdt);

        emailEdt.setText(email);
        fullNameEdt.setText(fullName);

        builder.setView(dialogView);
        final AlertDialog dialog = builder.create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

        final CircularProgressButton updateBtn = dialogView.findViewById(R.id.updateBtn);
        updateBtn.setOnClickListener(view -> {
            final String email = emailEdt.getText().toString();
            final String fullName = fullNameEdt.getText().toString();

            if (fullName.trim().isEmpty()) {
                TastyToast.makeText(requireContext(), "Full name must not be empty", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                        .show();
                return;
            }
            if (email.trim().isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                TastyToast.makeText(requireContext(), "Email must be a valid email address", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                        .show();
                return;
            }
            updateBtn.startAnimation();
            String id = user.getUid();

            Map<String, Object> obj = new HashMap<>();
            obj.put("fullName", fullName);
            obj.put("email", email);

            db.collection("users").document(id).update(obj).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        updateBtn.revertAnimation();
                        nameTxt.setText(fullName);
                        emailTxt.setText(email);
                        dialog.dismiss();
                        TastyToast.makeText(requireContext(), "Details changed successfully", TastyToast.LENGTH_LONG, TastyToast.SUCCESS)
                                .show();
                    }
                }
            });
        });
        dialog.show();
    }

    private void logoutUser() {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(getActivity(), StartActivity.class));
        requireActivity().finish();
    }

    private void uploadImage(Uri uri) {
        loadingImageProgress.setVisibility(View.VISIBLE);
        StorageReference ref
                = FirebaseStorage.getInstance().getReference()
                .child(
                        "images/"
                                + UUID.randomUUID().toString());
        ref.putFile(uri).addOnCompleteListener(task -> {
            while (!task.isComplete());
            if (task.isSuccessful()) {
                ref.getDownloadUrl().addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {
                        Uri uri1 = task1.getResult();
                        String id = user.getUid();

                        Map<String, Object> map = new HashMap<>();
                        assert uri1 != null;
                        map.put("photoUrl", uri1.toString());

                        db.collection("users").document(id).update(map).addOnCompleteListener(task2 -> {
                            if (task2.isSuccessful()) {
                                TastyToast.makeText(getContext(), "Updated profile picture successfully", TastyToast.LENGTH_LONG, TastyToast.SUCCESS)
                                .show();
                            } else {
                                TastyToast.makeText(getContext(), "Unable to update profile picture", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                                        .show();
                            }
                            loadingImageProgress.setVisibility(View.GONE);
                        });
                    }
                });
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null
                && data.getData() != null) {
            Uri imageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore
                        .Images
                        .Media
                        .getBitmap(
                                requireActivity().getContentResolver(),
                                imageUri);
                userImageView.setImageBitmap(bitmap);
                uploadImage(imageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.change_photo:
                changePhoto();
                break;
            case R.id.edit_email:
            case R.id.edit_name:
                changeDetails();
                break;
            case R.id.logoutBtn:
                logoutUser();
                break;
        }
    }
}