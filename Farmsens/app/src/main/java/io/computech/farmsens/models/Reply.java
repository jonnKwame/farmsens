package io.computech.farmsens.models;

import com.google.firebase.Timestamp;

public class Reply {
    private String id;
    private String userId;
    private String comment;
    private Timestamp datetime;

    public Reply() {
        // required empty constructor
    }

    public Reply(String id, String userId, String comment, Timestamp datetime) {
        this.id = id;
        this.userId = userId;
        this.comment = comment;
        this.datetime = datetime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    public Timestamp getDatetime() {
        return datetime;
    }
}
