package io.computech.farmsens.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import io.computech.farmsens.R;
import io.computech.farmsens.fragments.HomeFragment;
import io.computech.farmsens.fragments.ProfileFragment;
import io.computech.farmsens.fragments.CommunityFragment;
import io.computech.farmsens.fragments.TestFragment;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        SmoothBottomBar smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(i -> {
            switch (i) {
                case 0:
                    getCurrentFragment(new HomeFragment());
                    break;
                case 1:
                    getCurrentFragment(new TestFragment());
                    break;
                case 2:
                    getCurrentFragment(new CommunityFragment());
                    break;
                case 3:
                    getCurrentFragment(new ProfileFragment());
                    break;
            }

            return true;
        });

        // set default fragment to home
        getCurrentFragment(new HomeFragment());
    }

    private void getCurrentFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.nav_container, fragment)
                .commit();
    }
}
