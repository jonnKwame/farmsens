package io.computech.farmsens.activities.sub_activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.sdsmdg.tastytoast.TastyToast;

import java.util.Date;
import java.util.Objects;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import io.computech.farmsens.R;
import io.computech.farmsens.adapters.ReplyAdapter;
import io.computech.farmsens.models.Reply;

public class FeedActivity extends AppCompatActivity {
    private LinearLayout mainLayout;
    private ProgressBar loader;
    private Toolbar toolbar;
    private ImageView toolbarImage;
    private RecyclerView repliesRecycler;
    private EditText commentEdt;
    private TextView descriptionTxt;
    private CircularProgressButton submitBtn;
    private AppBarLayout appBarLayout;

    private FirebaseFirestore db;
    private ReplyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        db = FirebaseFirestore.getInstance();

        initUI();
        toolbar.setTitleTextColor(getResources().getColor(R.color.whiteTextColor));
        setSupportActionBar(toolbar);

        loadFeed();

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        submitBtn.setOnClickListener(view -> submitComment());
    }

    private void initUI() {
        mainLayout = findViewById(R.id.main_layout);
        loader = findViewById(R.id.loader);
        toolbar = findViewById(R.id.detail_toolbar);
        toolbarImage = findViewById(R.id.image_toolbar);
        commentEdt = findViewById(R.id.comment_edt);
        descriptionTxt = findViewById(R.id.description_txt);
        submitBtn = findViewById(R.id.submit_btn);
        repliesRecycler = findViewById(R.id.replies_recycler);
        appBarLayout = findViewById(R.id.app_bar);
    }

    private void loadFeed() {
        String feedId = getIntent().getStringExtra("feedId");
        assert feedId != null;
        db.collection("feed").document(feedId).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot doc = task.getResult();
                assert  doc != null;

                toolbar.setTitle(doc.getString("title"));
                Objects.requireNonNull(getSupportActionBar()).setTitle(doc.getString("title"));
                Glide.with(this)
                        .load(doc.getString("imageURL"))
                        .into(toolbarImage);
                descriptionTxt.setText(doc.getString("description"));

                Query query = FirebaseFirestore.getInstance().collection("feed").document(feedId).collection("replies").orderBy("datetime", Query.Direction.DESCENDING);

                FirestoreRecyclerOptions<Reply> options = new FirestoreRecyclerOptions.Builder<Reply>()
                        .setQuery(query, Reply.class)
                        .build();
                adapter = new ReplyAdapter(options, this);
                repliesRecycler.setLayoutManager(new LinearLayoutManager(this));
                repliesRecycler.setAdapter(adapter);

                adapter.startListening();

                appBarLayout.setVisibility(View.VISIBLE);
                mainLayout.setVisibility(View.VISIBLE);
                loader.setVisibility(View.GONE);
            } else {
                TastyToast.makeText(this, "Unable to load feed details", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                        .show();
            }
        });
    }

    private void submitComment() {
        String comment = commentEdt.getText().toString();
        if (comment.trim().isEmpty()) {
            TastyToast.makeText(this, "Comment cannot be empty", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                    .show();
            return;
        }

        submitBtn.startAnimation();
        String feedId = getIntent().getStringExtra("feedId");
        String id = db.collection("feed").document(feedId).collection("replies").document().getId();
        Reply reply = new Reply(id, FirebaseAuth.getInstance().getCurrentUser().getUid(), comment, new Timestamp(new Date()));

        db.collection("feed").document(feedId).collection("replies").document(id).set(reply).addOnCompleteListener(task -> {
           if (task.isSuccessful()) {
               adapter.notifyDataSetChanged();
               TastyToast.makeText(this, "Reply added", TastyToast.LENGTH_LONG, TastyToast.SUCCESS)
                       .show();
               commentEdt.setText("");

           } else {
               TastyToast.makeText(this, "Unable to add reply", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                       .show();

           }
            submitBtn.revertAnimation();
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        finish();
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        adapter.stopListening(); // remove listener when application stops or isn't in foreground to avoid memory leaks
        super.onStop();
    }
}
