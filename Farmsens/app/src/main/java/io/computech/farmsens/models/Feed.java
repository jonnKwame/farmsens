package io.computech.farmsens.models;

import com.google.firebase.Timestamp;

public class Feed {
    private String id; // feed id
    private String userId; // id of user who created feed
    private String title; // the title of the message
    private String description; // the message itself
    private Timestamp datetime;
    private String imageURL; // an optional image url for the feed

    public Feed() {
        // required empty constructor
    }

    public Feed(String id, String userId, String description, String title, Timestamp datetime, String imageURL) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.datetime = datetime;
        this.imageURL = imageURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
