package io.computech.farmsens.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import io.computech.farmsens.R;
import io.computech.farmsens.fragments.sub_home_fragments.CropsListFragment;
import io.computech.farmsens.fragments.sub_home_fragments.DeviceListFragment;
import io.computech.farmsens.fragments.sub_home_fragments.TestListFragment;


public class HomeFragment extends Fragment {

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // set action bar title
        requireActivity().setTitle("Home");

        // set pager adapter
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getChildFragmentManager(), FragmentPagerItems.with(requireContext())
                .add("Crops List", CropsListFragment.class)
                .add("Device", DeviceListFragment.class)
                .add("Test History", TestListFragment.class)
                .create()
        );
        ViewPager viewPager = view.findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = view.findViewById(R.id.viewpagerTab);
        viewPagerTab.setViewPager(viewPager);
    }
}