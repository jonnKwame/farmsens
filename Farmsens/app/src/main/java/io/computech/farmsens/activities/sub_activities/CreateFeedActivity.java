package io.computech.farmsens.activities.sub_activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.sdsmdg.tastytoast.TastyToast;

import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import io.computech.farmsens.R;
import io.computech.farmsens.models.Feed;

public class CreateFeedActivity extends AppCompatActivity {
    private EditText titleEdt, descriptionEdt;
    private ImageView selectedImageView;

    private Uri imageUri;

    private static final int REQUEST_CODE_SELECT_IMAGE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_feed);

        setTitle("Create New Feed");
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        // initialize ui elements
        titleEdt = findViewById(R.id.title_edt);
        descriptionEdt = findViewById(R.id.description_edt);
        selectedImageView = findViewById(R.id.selected_img_view);

        ImageButton addImageBtn = findViewById(R.id.add_image_btn);
        addImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dexter.withActivity(CreateFeedActivity.this)
                        .withPermissions(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        ).withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            if (intent.resolveActivity(getPackageManager()) != null) {
                                startActivityForResult(intent, REQUEST_CODE_SELECT_IMAGE);
                            }
                        } else {
                            TastyToast.makeText(getApplicationContext(),"Storage permission is needed to choose an image", TastyToast.LENGTH_LONG,TastyToast.INFO)
                                    .show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        TastyToast.makeText(getApplicationContext(),"Storage permission is needed to choose an image", TastyToast.LENGTH_LONG,TastyToast.INFO)
                                .show();
                    }
                }).check();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SELECT_IMAGE && resultCode == RESULT_OK) {
            if (data != null) {
                Uri selectedImageUri = data.getData();
                imageUri = selectedImageUri;

                if (selectedImageUri != null) {
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(selectedImageUri);
                        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                        selectedImageView.setImageBitmap(bitmap);
                    } catch (Exception e) {
                        TastyToast.makeText(getApplicationContext(), e.getMessage(), TastyToast.LENGTH_LONG,TastyToast.ERROR)
                                .show();
                    }
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.create_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
                return true;

            case R.id.done:
                saveFeed();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String getMimeType(Context context, Uri uri) {
        String extension;

        //Check uri format to avoid null
        if (Objects.equals(uri.getScheme(), ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(Objects.requireNonNull(uri.getPath()))).toString());

        }

        return extension;
    }

    private void saveFeed() {
        final String title = titleEdt.getText().toString();
        final String description = descriptionEdt.getText().toString();

        if (title.trim().isEmpty() || description.trim().isEmpty()) {
            TastyToast.makeText(this, "Both title and description are required", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                    .show();
            return;
        }

        final String userId = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        if (imageUri != null) {
            // image exists, save to storage
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle("Uploading...")
                    .setMessage("0%")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    })
                    .create();
            dialog.show();
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
            StorageReference storageReference = FirebaseStorage.getInstance()
                    .getReference().child("uploads/" + System.currentTimeMillis() + "." + getMimeType(this, imageUri));
            storageReference.putFile(imageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Task<Uri> uri = Objects.requireNonNull(taskSnapshot.getStorage().getDownloadUrl());

                            while (!uri.isComplete());

                            TastyToast.makeText(getApplicationContext(), "Saving feed post", TastyToast.LENGTH_LONG, TastyToast.INFO)
                                    .show();

                            final Uri url = uri.getResult();
                            final String id = db.collection("feed").document().getId();
                            assert url != null;
                            Feed feed = new Feed(id, userId, description, title, new Timestamp(new Date()), url.toString());
                            db.collection("feed").document(id).set(feed).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        TastyToast.makeText(getApplicationContext(),"Successfully created feed post", TastyToast.LENGTH_LONG, TastyToast.SUCCESS)
                                                .show();
                                        Intent intent = new Intent();
                                        setResult(RESULT_OK, intent);
                                        finish();
                                    }
                                }
                            });
                        }
                    }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                    dialog.setMessage(String.format("%.0f%%", progress));

                    if (progress == 100.0) dialog.dismiss();
                }
            });
        } else {
            TastyToast.makeText(getApplicationContext(), "Saving feed post", TastyToast.LENGTH_LONG, TastyToast.INFO)
                    .show();

            final String id = db.collection("feed").document().getId();
            Feed feed = new Feed(id, userId, description, title, new Timestamp(new Date()), "");
            db.collection("feed").document(id).set(feed).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        TastyToast.makeText(getApplicationContext(),"Successfully created feed post", TastyToast.LENGTH_LONG, TastyToast.SUCCESS)
                                .show();
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
            });
        }
    }
}