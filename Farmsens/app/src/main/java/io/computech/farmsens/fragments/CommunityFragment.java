package io.computech.farmsens.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.robertlevonyan.views.customfloatingactionbutton.FloatingActionButton;

import io.computech.farmsens.R;
import io.computech.farmsens.activities.sub_activities.CreateFeedActivity;
import io.computech.farmsens.activities.sub_activities.FeedActivity;
import io.computech.farmsens.adapters.FeedAdapter;
import io.computech.farmsens.models.Feed;

import static android.app.Activity.RESULT_OK;

public class CommunityFragment extends Fragment implements FeedAdapter.FeedListener {
    private FeedAdapter adapter;
    private ImageView emptyMessagesView;
    private TextView emptyMessagesTextView;

    private static final int REQUEST_CODE_ADD_NEW_FEED = 99;

    public CommunityFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_community, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // set action bar title
        requireActivity().setTitle("Community");

        emptyMessagesView = view.findViewById(R.id.empty_messages_img);
        emptyMessagesTextView = view.findViewById(R.id.empty_messages_text);

        RecyclerView messagesRecycler = view.findViewById(R.id.messages_recycler);
        final SwipeRefreshLayout refreshLayout = view.findViewById(R.id.communityRefreshLayout);
        refreshLayout.setRefreshing(true);

        Query query = FirebaseFirestore.getInstance().collection("feed").orderBy("datetime", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<Feed> options = new FirestoreRecyclerOptions.Builder<Feed>()
                .setQuery(query, Feed.class)
                .build();

        adapter = new FeedAdapter(options, requireContext(), this);
        messagesRecycler.setLayoutManager(new LinearLayoutManager(requireContext()));
        messagesRecycler.setAdapter(adapter);

        /*
        check if query returned 0 results and display empty text and image
         */
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);

                int totalItems = adapter.getItemCount();
                if (totalItems == 0) {
                    emptyMessagesTextView.setVisibility(View.VISIBLE);
                    emptyMessagesView.setVisibility(View.VISIBLE);
                } else {
                    refreshLayout.setRefreshing(false);
                    emptyMessagesTextView.setVisibility(View.GONE);
                    emptyMessagesView.setVisibility(View.GONE);
                }
            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshLayout.setRefreshing(false);
            }
        });

        // set up floating action button
        FloatingActionButton fab = view.findViewById(R.id.new_feed_btn);
        fab.setOnClickListener(view1 -> startActivityForResult(new Intent(requireContext(), CreateFeedActivity.class), REQUEST_CODE_ADD_NEW_FEED));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_ADD_NEW_FEED && resultCode == RESULT_OK) {
            adapter.stopListening();
            adapter.startListening();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening(); // listen to data items in real time
    }

    @Override
    public void onStop() {
        adapter.stopListening(); // remove listener when application stops or isn't in foreground to avoid memory leaks
        super.onStop();
    }

    @Override
    public void onItemClick(Feed feed) {
        Intent intent = new Intent(requireActivity(), FeedActivity.class);
        intent.putExtra("feedId", feed.getId());
        startActivity(intent);
    }
}