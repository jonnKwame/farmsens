package io.computech.farmsens.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Date;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import io.computech.farmsens.R;
import io.computech.farmsens.models.Reply;

public class ReplyAdapter extends FirestoreRecyclerAdapter<Reply, ReplyAdapter.ReplyViewHolder> {
    private Context context;
    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public ReplyAdapter(@NonNull FirestoreRecyclerOptions<Reply> options, Context context) {
        super(options);
        this.context = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull ReplyViewHolder holder, int position, @NonNull Reply model) {
        holder.bind(model);
    }

    @NonNull
    @Override
    public ReplyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reply, parent, false);
        return new ReplyViewHolder(view);
    }

    public class ReplyViewHolder extends RecyclerView.ViewHolder {
        private TextView commentTxt, timeAgoTxt, usernameTxt;
        private CircleImageView userPhoto;

        public ReplyViewHolder(@NonNull View itemView) {
            super(itemView);
            commentTxt = itemView.findViewById(R.id.comment_txt);
            timeAgoTxt = itemView.findViewById(R.id.time_ago_txt);
            usernameTxt = itemView.findViewById(R.id.username_txt);
            userPhoto = itemView.findViewById(R.id.user_image_view);
        }

        public void bind(Reply reply) {
            commentTxt.setText(reply.getComment());

            // get the time ago date
            Date date = reply.getDatetime().toDate();
            PrettyTime prettyTime = new PrettyTime();
            timeAgoTxt.setText(prettyTime.format(date));

            setUserNameAndImage(reply.getUserId());
        }

        private void setUserNameAndImage(String id) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("users").document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        String fullName = Objects.requireNonNull(task.getResult()).getString("fullName");
                        String userImg = task.getResult().getString("photoUrl");

                        usernameTxt.setText(fullName);

                        assert userImg != null;
                        if (!userImg.isEmpty()) {
                            Glide.with(context)
                                    .load(userImg)
                                    .into(userPhoto);
                        } else {
                            Glide.with(context)
                                    .load(R.drawable.default_user)
                                    .into(userPhoto);
                        }
                    } else {
                        Glide.with(context)
                                .load(R.drawable.default_user)
                                .into(userPhoto);
                        usernameTxt.setText("Anonymous");
                    }
                }
            });

        }
    }
}
