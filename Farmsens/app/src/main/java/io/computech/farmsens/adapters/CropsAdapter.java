package io.computech.farmsens.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;
import java.util.Objects;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import io.computech.farmsens.R;
import io.computech.farmsens.models.Crop;

public class CropsAdapter extends FirestoreRecyclerAdapter<Crop, CropsAdapter.CropsViewHolder> {
    private Context context;
    private CropListener listener;

    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public CropsAdapter(@NonNull FirestoreRecyclerOptions<Crop> options, Context context,  CropListener listener) {
        super(options);
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onBindViewHolder(@NonNull CropsViewHolder holder, int position, @NonNull Crop model) {
        holder.bind(model);
    }

    @NonNull
    @Override
    public CropsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_crop, parent, false);
        return new CropsViewHolder(view);
    }

    public class CropsViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView nameTxt;
        private CircularProgressButton toggleBtn;

        public CropsViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image_url);
            nameTxt = itemView.findViewById(R.id.name_txt);
            toggleBtn = itemView.findViewById(R.id.crops_toggle);
        }

        public void bind(Crop crop) {
            Glide.with(context)
                    .load(crop.getImageUrl())
                    .into(imageView);
            nameTxt.setText(crop.getName());

            renderButton(toggleBtn, crop);
        }

        @SuppressLint("SetTextI18n")
        private void renderButton(CircularProgressButton button, Crop crop) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            FirebaseFirestore db = FirebaseFirestore.getInstance();

            db.collection("crops").document(crop.getId()).collection("users").get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    List<DocumentSnapshot> docs = Objects.requireNonNull(task.getResult()).getDocuments();
                    for (DocumentSnapshot doc : docs) {
                        assert user != null;
                        if (doc.getId().equals(user.getUid())) {
                            button.setBackground(context.getDrawable(R.drawable.remove_btn));
                            button.setText("Remove crop");
                            button.setOnClickListener(view -> listener.removeCrop(crop, button));
                            return;
                        }
                    }
                    button.setBackground(context.getDrawable(R.drawable.onboarding_button));
                    button.setText("Add crop");
                    button.setOnClickListener(view -> listener.addCrop(crop, button));
                }
            });
        }
    }

    public interface CropListener {
        void addCrop(Crop crop, CircularProgressButton btn);
        void removeCrop(Crop crop, CircularProgressButton btn);
    }
}
