package io.computech.farmsens.config;

public class Config {
    public static final String AUTH_PREFS = "AUTH_PREFS";
    public static final String CURRENT_USER_USERNAME = "CURRENT_USER_USERNAME";
}
