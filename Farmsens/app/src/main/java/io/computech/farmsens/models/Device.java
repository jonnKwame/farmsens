package io.computech.farmsens.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Device {
    @SerializedName("gateway_id")
    private String gatewayId;

    private String name;
    private String id;
    private List<Sensors> sensors;

    public Device() {}

    public Device(String gatewayId, String name, String id, List<Sensors> sensors) {
        this.gatewayId = gatewayId;
        this.name = name;
        this.id = id;
        this.sensors = sensors;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Sensors> getSensors() {
        return sensors;
    }

    public void setGatewayId(String gatewayId) {
        this.gatewayId = gatewayId;
    }

    public String getGatewayId() {
        return gatewayId;
    }

    public void setSensors(List<Sensors> sensors) {
        this.sensors = sensors;
    }
}
