package io.computech.farmsens.listeners;

public interface QRCodeListener {
    void onQRCodeFound(String qrCode);
    void qrCodeNotFound();
}
