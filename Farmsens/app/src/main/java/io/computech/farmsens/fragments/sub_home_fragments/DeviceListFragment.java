package io.computech.farmsens.fragments.sub_home_fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.sdsmdg.tastytoast.TastyToast;

import java.io.IOException;
import java.util.Objects;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import io.computech.farmsens.R;
import io.computech.farmsens.activities.sub_activities.QRActivity;
import io.computech.farmsens.listeners.ApiInterface;
import io.computech.farmsens.models.Device;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class DeviceListFragment extends Fragment {
    private CircularProgressButton registerDeviceBtn, unregisterBtn;
    private ImageView emptyDevicesImage;
    private CardView deviceCard;
    private TextView deviceIdTxt, nameTxt, gatewayTxt;
    private ProgressBar loader;
    private static int REQUEST_SCAN_CODE = 99;

    private FirebaseFirestore db;
    private FirebaseUser user;

    public DeviceListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_device_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initUI(view);

        db = FirebaseFirestore.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();

        getDevice();

        registerDeviceBtn.setOnClickListener(view1 -> {
            registerDeviceBtn.startAnimation();
            startActivityForResult(new Intent(requireActivity(), QRActivity.class), REQUEST_SCAN_CODE);
        });
    }

    @SuppressLint("SetTextI18n")
    private void getDevice() {
        db.collection("users").document(user.getUid()).collection("devices")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        if (Objects.requireNonNull(task.getResult()).getDocuments().isEmpty()) {
                            deviceCard.setVisibility(View.INVISIBLE);
                            emptyDevicesImage.setVisibility(View.VISIBLE);
                            registerDeviceBtn.setVisibility(View.VISIBLE);
                        } else {
                            DocumentSnapshot doc =  Objects.requireNonNull(task.getResult()).getDocuments().get(0);
                            if (doc.exists()) {
                                deviceIdTxt.setText(doc.getString("id"));
                                nameTxt.setText("FarmSens Soil Meter");
                                gatewayTxt.setText(doc.getString("gatewayId"));

                                deviceCard.setVisibility(View.VISIBLE);

                                unregisterBtn.setOnClickListener(v -> {
                                    unregisterBtn.startAnimation();
                                    db.collection("users").document(user.getUid())
                                            .collection("devices")
                                            .document(doc.getId())
                                            .delete()
                                            .addOnCompleteListener(task1 -> {
                                                if (task.isSuccessful()) {
                                                    unregisterBtn.revertAnimation();
                                                    TastyToast.makeText(requireContext(), "Device unregistered successfully", TastyToast.LENGTH_LONG, TastyToast.SUCCESS)
                                                            .show();
                                                    getDevice();
                                                } else {
                                                    TastyToast.makeText(requireContext(), "Unable to unregister device, try again later", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                                                            .show();
                                                }
                                            });
                                });

                                emptyDevicesImage.setVisibility(View.INVISIBLE);
                                registerDeviceBtn.setVisibility(View.INVISIBLE);
                            } else {
                                emptyDevicesImage.setVisibility(View.VISIBLE);
                                registerDeviceBtn.setVisibility(View.VISIBLE);
                            }
                        }
                        registerDeviceBtn.revertAnimation();
                    } else {
                        TastyToast.makeText(requireContext(), "Unable to fetch device information",  TastyToast.LENGTH_LONG, TastyToast.ERROR)
                                .show();
                    }
                    loader.setVisibility(View.GONE);
                });
    }

    private void initUI(View view) {
        registerDeviceBtn = view.findViewById(R.id.register_device_btn);
        unregisterBtn = view.findViewById(R.id.unregister_btn);
        emptyDevicesImage = view.findViewById(R.id.empty_devices_img);
        loader = view.findViewById(R.id.loader);
        deviceCard = view.findViewById(R.id.device_card);
        nameTxt = view.findViewById(R.id.name_txt);
        deviceIdTxt = view.findViewById(R.id.device_id_txt);
        gatewayTxt = view.findViewById(R.id.gateway_id_txt);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_SCAN_CODE && resultCode == RESULT_OK && data != null) {
            String qrCode = data.getStringExtra("qrCode");

            assert qrCode != null;
            String[] idx = qrCode.split("/");

            String deviceId = idx[idx.length - 1];
            Log.d("QR_CODE", deviceId);

            Call<Device> getDeviceInfo = ApiInterface.create().getDeviceInfo(deviceId);
            getDeviceInfo.enqueue(new Callback<Device>() {
                @Override
                public void onResponse(@NonNull Call<Device> call, @NonNull Response<Device> response) {
                    if (response.isSuccessful()) {
                        Device device = response.body();
                        assert device != null;

                        db.collection("users").document(user.getUid()).collection("devices")
                                .add(device)
                                .addOnCompleteListener(task -> {
                                    if (task.isSuccessful()) {
                                        TastyToast.makeText(requireContext(), "Device registered successfully", TastyToast.LENGTH_LONG, TastyToast.SUCCESS)
                                                .show();
                                        getDevice();
                                    }
                                });
                    } else {
                        try {
                            Log.d("QR_CODE", response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        TastyToast.makeText(requireContext(), "Unable to register device", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                                .show();
                        getDevice();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Device> call, @NonNull Throwable t) {
                    Log.d("QR_CODE", t.getMessage());
                    TastyToast.makeText(requireContext(), "Unable to register device", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                            .show();
                    getDevice();
                }
            });
        } else {
            registerDeviceBtn.revertAnimation();
        }
    }
}
