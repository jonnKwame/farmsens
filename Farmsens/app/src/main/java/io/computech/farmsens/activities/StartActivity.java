package io.computech.farmsens.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.sdsmdg.tastytoast.TastyToast;

import org.angmarch.views.NiceSpinner;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import io.computech.farmsens.R;
import io.computech.farmsens.models.User;

public class StartActivity extends AppCompatActivity {

    private LinearLayout dotsLayout;
    private int[] layouts;
    private CircularProgressButton loginBtn;

    private static int RC_SIGN_IN = 999;

    //firebase auth instance
    private FirebaseAuth auth;

    //fire store instance
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //check if user isn't already logged in
        //initialize firebase
        auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();

        if (user != null) {
            startActivity(new Intent(StartActivity.this, HomeActivity.class));
            finish();
        }

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_start);

        ViewPager viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.layoutDots);
        loginBtn = findViewById(R.id.loginBtn);

        layouts = new int[]{
                R.layout.welcome_slide1,
                R.layout.welcome_slide2,
                R.layout.welcome_slide3
        };

        // adding bottom dots
        addBottomDots(0);

        db = FirebaseFirestore.getInstance();

        MyViewPagerAdapter myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        loginBtn.setOnClickListener(view -> {
            List<AuthUI.IdpConfig> providers = Collections.singletonList(
                    new AuthUI.IdpConfig.PhoneBuilder().build()
            );
            loginBtn.startAnimation();

            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(providers)
                            .setLogo(R.drawable.logo)
                            .build(),
                    RC_SIGN_IN
            );
        });
    }

    private void addBottomDots(int currentPage) {
        TextView[] dots = new TextView[layouts.length];

        LinearLayout.LayoutParams textLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textLayoutParams.setMargins(5, 0, 0, 0);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setWidth(30);
            dots[i].setHeight(5);
            dots[i].setLayoutParams(textLayoutParams);
            dots[i].setBackground(getResources().getDrawable(R.drawable.onboarding_dotunselected));
            dots[i].setTextSize(35);
            dotsLayout.addView(dots[i]);
        }

        dots[currentPage].setWidth(70);
        dots[currentPage].setBackground(getResources().getDrawable(R.drawable.onboarding_dot_selected));
    }


    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {

        MyViewPagerAdapter() {
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            assert layoutInflater != null;
            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                final FirebaseUser user = auth.getCurrentUser();

                // check if user does not already exist
                assert user != null;
                db.collection("users").document(user.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot doc = task.getResult();
                            assert doc != null;
                            if (doc.exists()) {
                                // user already exists, proceed to main screen
                                TastyToast.makeText(getApplicationContext(), "Successfully signed in", TastyToast.LENGTH_LONG, TastyToast.SUCCESS)
                                        .show();
                                loginBtn.revertAnimation();
                                startActivity(new Intent(StartActivity.this, HomeActivity.class));
                                finish();
                            } else registerNewUser(user.getUid(), user.getPhoneNumber());
                        } else registerNewUser(user.getUid(), user.getPhoneNumber());
                    }
                });

            } else {
                TastyToast.makeText(this, "Signing in failed...", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
                loginBtn.revertAnimation();
            }
        }
    }

    private void registerNewUser(final String id, final String phone) {
        AlertDialog.Builder builder = new AlertDialog.Builder(StartActivity.this);
        final View dialogView = LayoutInflater.from(this).inflate(
                R.layout.dialog_register_new_user,
                (ViewGroup) findViewById(R.id.registerUserDialog)
        );

        List<String> dataset = new LinkedList<>(Arrays.asList("Prefer Not To Say", "Male", "Female"));
        ((NiceSpinner) dialogView.findViewById(R.id.nice_spinner)).attachDataSource(dataset);

        builder.setView(dialogView);
        final AlertDialog dialog = builder.create();

        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

        final CircularProgressButton registerBtn = dialogView.findViewById(R.id.registerBtn);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText emailEdt = dialogView.findViewById(R.id.emailEdt);
                EditText fullNameEdt = dialogView.findViewById(R.id.fullNameEdt);
                String sex = ((NiceSpinner) dialogView.findViewById(R.id.nice_spinner)).getSelectedItem().toString();

                if (fullNameEdt.getText().toString().trim().isEmpty() || sex.trim().isEmpty()) {
                    TastyToast.makeText(getApplicationContext(), "Please provide your full name and sex", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                            .show();
                    return;
                }
                if (!emailEdt.getText().toString().trim().isEmpty() && !Patterns.EMAIL_ADDRESS.matcher(emailEdt.getText().toString()).matches()) {
                    TastyToast.makeText(getApplicationContext(),"Email must be a valid email address", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                            .show();
                    return;
                }
                registerBtn.startAnimation();
                User user;

                if (emailEdt.getText().toString().isEmpty()) {
                 user = new User(id, fullNameEdt.getText().toString(), "", sex, phone);
                } else {
                    user = new User(id, fullNameEdt.getText().toString(), emailEdt.getText().toString(), sex, phone);
                }

                //save user to firebase
                db.collection("users").document(id).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            TastyToast.makeText(getApplicationContext(), "Successfully created new account", TastyToast.LENGTH_LONG, TastyToast.SUCCESS)
                                    .show();
                            registerBtn.revertAnimation();
                            loginBtn.revertAnimation();
                            dialog.dismiss();
                            startActivity(new Intent(StartActivity.this, HomeActivity.class));
                            finish();
                        }
                    }
                });
            }
        });

        dialog.show();
    }
}
