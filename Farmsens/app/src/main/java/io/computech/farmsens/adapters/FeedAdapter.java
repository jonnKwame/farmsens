package io.computech.farmsens.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Date;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import io.computech.farmsens.R;
import io.computech.farmsens.models.Feed;

public class FeedAdapter extends FirestoreRecyclerAdapter<Feed, FeedAdapter.FeedViewHolder> {
    private Context context;
    private FeedListener listener;

    public FeedAdapter(@NonNull FirestoreRecyclerOptions<Feed> options, Context context, FeedListener listener) {
        super(options);
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onBindViewHolder(@NonNull FeedViewHolder holder, int position, @NonNull Feed model) {
        holder.bind(model);
    }

    @NonNull
    @Override
    public FeedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feed, parent, false);
        return new FeedViewHolder(view);
    }

    class FeedViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private CircleImageView userImageView;
        private TextView usernameTxt, timeAgoTxt, titleTxt, descriptionTxt, resultsLength, likesLength;

        FirebaseFirestore db;

        public FeedViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image_view);
            userImageView = itemView.findViewById(R.id.user_image_view);
            usernameTxt = itemView.findViewById(R.id.username_txt);
            timeAgoTxt = itemView.findViewById(R.id.time_ago_txt);
            titleTxt = itemView.findViewById(R.id.title_txt);
            descriptionTxt = itemView.findViewById(R.id.description_txt);
            resultsLength = itemView.findViewById(R.id.results_length);

            itemView.setOnClickListener(view -> listener.onItemClick(getItem(getAdapterPosition())));
        }

        public void bind(Feed feed) {
            titleTxt.setText(feed.getTitle());
            descriptionTxt.setText(feed.getDescription());
            setUserNameAndImage(feed.getUserId());

            if (feed.getImageURL() != null && !feed.getImageURL().isEmpty()) {
                Glide.with(context)
                        .load(feed.getImageURL())
                        .into(imageView);
            } else {
                imageView.setVisibility(View.GONE);
            }

            // get the time ago date
            Date date = feed.getDatetime().toDate();
            PrettyTime prettyTime = new PrettyTime();
            timeAgoTxt.setText(prettyTime.format(date));

            getResultsLength(feed.getId());
        }

        private void getResultsLength(String id) {
            db = FirebaseFirestore.getInstance();
            db.collection("feed").document(id).collection("replies").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        int size = Objects.requireNonNull(task.getResult()).size();
                        if (size == 1) {
                            resultsLength.setText(size + " reply");
                        } else {
                            resultsLength.setText(size + " replies");
                        }
                    }
                }
            });
        }

        private void setUserNameAndImage(String id) {
            db = FirebaseFirestore.getInstance();
            db.collection("users").document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        String fullName = Objects.requireNonNull(task.getResult()).getString("fullName");
                        String userImg = task.getResult().getString("photoUrl");

                        usernameTxt.setText(fullName);

                        assert userImg != null;
                        if (!userImg.isEmpty()) {
                            Glide.with(context)
                                    .load(userImg)
                                    .into(userImageView);
                        } else {
                            Glide.with(context)
                                    .load(R.drawable.default_user)
                                    .into(userImageView);
                        }
                    } else {
                        Glide.with(context)
                                .load(R.drawable.default_user)
                                .into(userImageView);
                        usernameTxt.setText("Anonymous");
                    }
                }
            });

        }
    }

    public interface FeedListener {
        void onItemClick(Feed feed);
    }
}
