package io.computech.farmsens.models;

import com.google.gson.annotations.SerializedName;

public class Sensors {
    private String name;
    private String id;
    private Value value;

    public Sensors() {}

    public Sensors(String name, String id, Value value) {
        this.name = name;
        this.id = id;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public class Value {
        @SerializedName("date_received")
        private String dateReceived;

        private double value;
        private String timeStamp;

        public Value() {}

        public Value(String dateReceived, double value, String timeStamp) {
            this.dateReceived = dateReceived;
            this.value = value;
            this.timeStamp = timeStamp;
        }

        public double getValue() {
            return value;
        }

        public String getDateReceived() {
            return dateReceived;
        }

        public String getTimeStamp() {
            return timeStamp;
        }

        public void setDateReceived(String dateReceived) {
            this.dateReceived = dateReceived;
        }

        public void setTimeStamp(String timeStamp) {
            this.timeStamp = timeStamp;
        }

        public void setValue(double value) {
            this.value = value;
        }
    }
}
