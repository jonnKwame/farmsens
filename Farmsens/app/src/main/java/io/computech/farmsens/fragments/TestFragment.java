package io.computech.farmsens.fragments;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.sdsmdg.tastytoast.TastyToast;

import java.util.ArrayList;
import java.util.Objects;

import io.computech.farmsens.R;
import io.computech.farmsens.listeners.ApiInterface;
import io.computech.farmsens.models.Device;
import io.computech.farmsens.models.Sensors;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestFragment extends Fragment {
    private PieChart chart;
    private ProgressBar loader;
    private TextView noDeviceTxt;
    private ImageView noDeviceImg;

    FirebaseFirestore db;
    FirebaseUser user;

    public TestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_test, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // set action bar title
        requireActivity().setTitle("My Farm");

        db = FirebaseFirestore.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();

        initUI(view);

        loadDeviceTest();
    }

    private void loadDeviceTest() {
        db.collection("users").document(user.getUid()).collection("devices").get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        if (Objects.requireNonNull(task.getResult()).getDocuments().isEmpty()) {
                            noDeviceImg.setVisibility(View.VISIBLE);
                            noDeviceTxt.setVisibility(View.VISIBLE);
                            chart.setVisibility(View.GONE);
                            loader.setVisibility(View.GONE);
                        } else {
                            DocumentSnapshot doc =  Objects.requireNonNull(task.getResult()).getDocuments().get(0);
                            if (doc.exists()) {
                                loadChartData(doc.getString("id"));
                            } else {
                                noDeviceImg.setVisibility(View.VISIBLE);
                                noDeviceTxt.setVisibility(View.VISIBLE);
                                chart.setVisibility(View.GONE);
                                loader.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        TastyToast.makeText(requireContext(), "Unable to fetch test data", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                                .show();
                        noDeviceImg.setVisibility(View.VISIBLE);
                        noDeviceTxt.setVisibility(View.VISIBLE);
                        chart.setVisibility(View.GONE);
                        loader.setVisibility(View.GONE);
                    }
                });
    }

    private void loadChartData(String id) {
        Call<Device> getDeviceInfo = ApiInterface.create().getDeviceInfo(id);
        getDeviceInfo.enqueue(new Callback<Device>() {
            @Override
            public void onResponse(@NonNull Call<Device> call, @NonNull Response<Device> response) {
                if (response.isSuccessful()) {
                    Device device = response.body();

                    ArrayList<PieEntry> entries = new ArrayList<>();
                    assert device != null;
                    for (Sensors sensor : device.getSensors()) {
                        if (sensor.getName().equals("FT")) {
                            entries.add(new PieEntry((float) sensor.getValue().getValue(), "Soil Fertility"));
                        } else if (sensor.getName().equals("SM")) {
                            entries.add(new PieEntry((float) sensor.getValue().getValue(), "Soil Moisture"));
                        } else if (sensor.getName().equals("PH")){
                            entries.add(new PieEntry((float) sensor.getValue().getValue(), "Soil PH"));
                        } else {
                            entries.add(new PieEntry((float) sensor.getValue().getValue(), "Soil Temperature"));
                        }
                    }
                    PieDataSet pieDataSet = new PieDataSet(entries, "Test Results");
                    pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
                    pieDataSet.setValueTextColor(Color.BLACK);
                    pieDataSet.setValueTextSize(16f);

                    PieData pieData = new PieData(pieDataSet);

                    chart.setData(pieData);

                    Description description = new Description();
                    description.setText("Showing results from recent test performed");
                    chart.setDescription(description);

                    chart.getDescription().setEnabled(true);
                    chart.setCenterText("Test Results");
                    chart.animate();

                    noDeviceImg.setVisibility(View.GONE);
                    noDeviceTxt.setVisibility(View.GONE);
                    chart.setVisibility(View.VISIBLE);
                } else {
                    TastyToast.makeText(requireContext(), "Unable to fetch test info", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                            .show();
                    noDeviceImg.setVisibility(View.VISIBLE);
                    noDeviceTxt.setVisibility(View.VISIBLE);
                    chart.setVisibility(View.GONE);
                }
                loader.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<Device> call, @NonNull Throwable t) {
                TastyToast.makeText(requireContext(), "Unable to fetch test info", TastyToast.LENGTH_LONG, TastyToast.ERROR)
                        .show();
                noDeviceImg.setVisibility(View.VISIBLE);
                noDeviceTxt.setVisibility(View.VISIBLE);
                chart.setVisibility(View.GONE);
                loader.setVisibility(View.GONE);
            }
        });
    }

    private void initUI(View view) {
        chart = view.findViewById(R.id.chart);
        loader = view.findViewById(R.id.loader);
        noDeviceTxt = view.findViewById(R.id.no_device_txt);
        noDeviceImg = view.findViewById(R.id.no_device_img);
    }
}