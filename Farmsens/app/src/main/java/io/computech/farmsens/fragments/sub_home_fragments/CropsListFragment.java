package io.computech.farmsens.fragments.sub_home_fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.HashMap;
import java.util.Map;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import io.computech.farmsens.R;
import io.computech.farmsens.adapters.CropsAdapter;
import io.computech.farmsens.models.Crop;

public class CropsListFragment extends Fragment implements CropsAdapter.CropListener {
    private RecyclerView cropsRecycler;
    private ProgressBar loader;
    private CropsAdapter adapter;

    private FirebaseFirestore db;
    private FirebaseUser user;

    public CropsListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crops_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initUI(view);

        db = FirebaseFirestore.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();

        Query query = db.collection("crops").orderBy("name", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions<Crop> options = new FirestoreRecyclerOptions.Builder<Crop>()
                .setQuery(query, Crop.class)
                .build();

        adapter = new CropsAdapter(options, requireContext(), this);
        cropsRecycler.setLayoutManager(new LinearLayoutManager(requireContext()));
        cropsRecycler.setAdapter(adapter);
        loader.setVisibility(View.GONE);

        adapter.startListening();
    }

    private void initUI(View view) {
        cropsRecycler = view.findViewById(R.id.crops_recycler);
        loader = view.findViewById(R.id.loader);
    }

    @Override
    public void addCrop(Crop crop, CircularProgressButton button) {
        Map<String, String> map = new HashMap<>();
        map.put("userId",  user.getUid());

        button.startAnimation();
        db.collection("crops").document(crop.getId()).collection("users")
                .document(user.getUid())
                .set(map)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        adapter.notifyDataSetChanged();
                    }
                    button.revertAnimation();
                });
    }

    @Override
    public void removeCrop(Crop crop, CircularProgressButton button) {
        db.collection("crops").document(crop.getId()).collection("users")
                .document(user.getUid())
                .delete()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        adapter.notifyDataSetChanged();
                    }
                    button.revertAnimation();
                });
    }
}
