package io.computech.farmsens.models;

public class User {
    private String id;
    private String fullName;
    private String email;
    private String photoUrl;
    private String phoneNumber;
    private String sex;

    //required empty constructor for firebase
    public User() {}

    public User(String id, String fullName, String email, String sex, String phoneNumber) {
        this(id, fullName, email, "", sex, phoneNumber);
    }

    public User(String id, String fullName, String email, String photoUrl, String sex, String phoneNumber) {
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.photoUrl = photoUrl;
        this.phoneNumber = phoneNumber;
        this.sex = sex;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhoneNumber(){
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
