#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <DHT.h>

// I2C  pins declaration
LiquidCrystal_I2C lcd(0x27,2,1,0,4,5,6,7,3,POSITIVE);

#define EC              A1
#define soil_moisture   A3 
#define scan_btn        8 
#define led_green       3
#define led_red         2
#define DHTPIN          5            // sensor is connected to digital I/O pin 5
#define DHTTYPE         DHT11       // DHT 11  (AM2302)

// Initialize DHT sensor for normal 16mhz Arduino
DHT dht(DHTPIN, DHTTYPE);

// declare variables for readings
float soil_moisture_results, soil_moisture_1 ,soil_moisture_2, soil_moisture_3 ;
float EC_results, EC_1 , EC_2 , EC_3 ;
float temperature_results, temperature_1, temperature_2, temperature_3 ;

void setup() {
  //setting pin modes
  pinMode(led_green,OUTPUT);      // setting pin D3 as output
  pinMode(led_red,  OUTPUT);      // setting pin D2 as output
  pinMode(scan_btn, INPUT );      // setting pin D8 as an input pin
  
  
  //initialize lcd screen with 16 columns and 2 rows
  lcd.begin(16,2);    
  // turn on the backlight
  lcd.backlight();

  // turn on the backlight
   dht.begin();
  
}

void loop() {
  // put your main code here, to run repeatedly:
      

  lcd.clear();
  lcd.setCursor(5,0);
  lcd.print("Farmsens");

  // connect_funct    "This function will connect the device to the gateway"



  /* Clears screen and indictes device is ready to be used*/
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Device Ready");

  // endless loop that runs while the device is on
  while(true)
      {
          // check if scan button has been pressed
            if(digitalRead(scan_btn)== HIGH)
               {
                  scan();
                  continue;
                }
      }  
}

/* Implementation of scan function */
void scan()
  {

      digitalWrite(led_green,HIGH);     // Turn green led on during scanning
      
      /* Clears screen and indictes device is reading parameters*/
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Testing Soil");

      /*Performing first test*/

      soil_moisture_1 = analogRead(soil_moisture);  // Take 1st soil moisture reading
      EC_1            = analogRead(EC);
      temperature_1   = dht.readTemperature();

      delay(60000) ;       // Delay for a minute before next test

      /*Performing second test*/
      soil_moisture_2 = analogRead(soil_moisture);  // Take 1st soil moisture reading
      EC_2            = analogRead(EC);
      temperature_2   = dht.readTemperature();

      delay(60000) ;       // Delay for a minute before next test

      /*Performing third test*/
      soil_moisture_3 = analogRead(soil_moisture);  // Take 1st soil moisture reading
      EC_3            = analogRead(EC);
      temperature_3   = dht.readTemperature();

      /* Strike average for soil results */

      soil_moisture_results = (soil_moisture_1 + soil_moisture_2 + soil_moisture_3)/3.00 ;
      EC_results            = ( EC_1 +  EC_2 +  EC_3 )/3.00;
      temperature_results   = (temperature_1 + temperature_2 + temperature_3)/3.00;

      //transmission() ; "Sends data to gateway"

      /* Clears screen and indictes device is done reading parameters*/
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Testing Completed");

      delay(2000);      // delay for 2 seconds

      /* Displaying results on screen in 1 decimal place*/ 
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Moisture:");
      lcd.print(soil_moisture_results,1);     // Display moisture readings  
      lcd.setCursor(0,1);
      lcd.print("EC:");
      lcd.print(EC_results,1);                // Display EC readings        
      lcd.print(" Tmp:");
      lcd.print(temperature_results,1);       // Display Temperature readings
      
       
      
   }
